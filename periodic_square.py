from firedrake import *
import numpy as np

'''
'''

__all__ = ['PeriodicUnitSquareMeshHierarchy', 'GTPeriodicUnitSquareMeshHierarchy']


class PeriodicUnitSquareMeshHierarchy(object):

    def __init__(self, n, nref):
        self.n = n
        self.nref = nref

    @staticmethod
    def periodise(m):
        coord_fs = VectorFunctionSpace(m, "DG", 1, dim=2)
        old_coordinates = m.coordinates
        new_coordinates = Function(coord_fs)
        domain = "{[i, j]: 0 <= i < old_coords.dofs and 0 <= j < new_coords.dofs}"
        instructions = """
        <float64> pi = 3.141592653589793
        <float64> eps = 1e-12
        <float64> bigeps = 1e-1
        <float64> Y = 0
        <float64> Z = 0
        for i
            Y = Y + old_coords[i, 1]
            Z = Z + old_coords[i, 2]
        end
        for j
            <float64> phi = atan2(old_coords[j, 1], old_coords[j, 0])
            <float64> _phi = fabs(sin(phi))
            <double> _theta_1 = atan2(old_coords[j, 2], old_coords[j, 1] / sin(phi) - 1)
            <double> _theta_2 = atan2(old_coords[j, 2], old_coords[j, 0] / cos(phi) - 1)
            <float64> theta = if(_phi > bigeps, _theta_1, _theta_2)
            new_coords[j, 0] = phi / (2 * pi)
            new_coords[j, 0] = if(new_coords[j, 0] < -eps, new_coords[j, 0] + 1, new_coords[j, 0])
            <float64> _nc_abs = fabs(new_coords[j, 0])
            new_coords[j, 0] = if(_nc_abs < eps and Y < 0, 1, new_coords[j, 0])
            new_coords[j, 1] = theta / (2 * pi)
            new_coords[j, 1] = if(new_coords[j, 1] < -eps, new_coords[j, 1] + 1, new_coords[j, 1])
            _nc_abs = fabs(new_coords[j, 1])
            new_coords[j, 1] = if(_nc_abs < eps and Z < 0, 1, new_coords[j, 1])
            new_coords[j, 0] = new_coords[j, 0] * Lx[0]
            new_coords[j, 1] = new_coords[j, 1] * Ly[0]
        end
        """
        cLx = Constant(1)
        cLy = Constant(1)

        par_loop((domain, instructions), dx,
                 {"new_coords": (new_coordinates, WRITE),
                  "old_coords": (old_coordinates, READ),
                  "Lx": (cLx, READ),
                  "Ly": (cLy, READ)},
                  is_loopy_kernel=True)

        return Mesh(new_coordinates)

    @staticmethod
    def snap(mesh, N, L=1):
        coords = mesh.coordinates.dat.data
        coords[...] = np.round((N/L)*coords)*(L/N)
        
    def mesh(self):
        distribution_parameters={"partition": True,
                                 "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}
        coarse_n = self.n/(2**self.nref)
        base_mesh = TorusMesh(coarse_n, coarse_n, 1.0, 0.5,
                              distribution_parameters=distribution_parameters)
        mh = MeshHierarchy(base_mesh,
                           self.nref,
                           distribution_parameters=distribution_parameters)

        meshes = tuple(self.periodise(m) for m in mh)
        mh = HierarchyBase(meshes,
                           mh.coarse_to_fine_cells,
                           mh.fine_to_coarse_cells,
                           nested=True)

        for (i, m) in enumerate(mh):
            if i > 0:
                self.snap(m, self.n * 2**i)
        return mh[-1]


class GTPeriodicUnitSquareMeshHierarchy(object):

    def __init__(self, n, nref):
        self.n = n
        self.nref = nref
        
        distribution_parameters = {"partition": True,
                                   "overlap_type": (DistributedMeshOverlapType.VERTEX, 2)}
        base_mesh = TorusMesh(self.n, self.n, 1.0, 0.5,
                              distribution_parameters=distribution_parameters)
        mh = MeshHierarchy(base_mesh,
                           self.nref,
                           distribution_parameters=distribution_parameters)

        meshes = tuple(self.periodise(m) for m in mh)

        gt_meshes = [meshes[-1], meshes[-1]]

        mh = HierarchyBase(meshes,
                           mh.coarse_to_fine_cells,
                           mh.fine_to_coarse_cells,
                           nested=True)

        for (i, m) in enumerate(mh):
            if i > 0:
                self.snap(m, self.n * 2**i)

        for (i, m) in enumerate(gt_meshes):
            if i > 0:
                self.snap(m, self.n * 2**i)

        gt_mh = NonnestedHierarchy(gt_meshes)

        self._coarse_mh = mh
        self._gt_mh = gt_mh

    @staticmethod
    def periodise(m):
        coord_fs = VectorFunctionSpace(m, "DG", 1, dim=2)
        old_coordinates = m.coordinates
        new_coordinates = Function(coord_fs)
        domain = "{[i, j]: 0 <= i < old_coords.dofs and 0 <= j < new_coords.dofs}"

        instructions = """
        <float64> pi = 3.141592653589793
        <float64> eps = 1e-12
        <float64> bigeps = 1e-1
        <float64> Y = 0
        <float64> Z = 0
        for i
            Y = Y + old_coords[i, 1]
            Z = Z + old_coords[i, 2]
        end
        for j
            <float64> phi = atan2(old_coords[j, 1], old_coords[j, 0])
            <float64> _phi = fabs(sin(phi))
            <double> _theta_1 = atan2(old_coords[j, 2], old_coords[j, 1] / sin(phi) - 1)
            <double> _theta_2 = atan2(old_coords[j, 2], old_coords[j, 0] / cos(phi) - 1)
            <float64> theta = if(_phi > bigeps, _theta_1, _theta_2)
            new_coords[j, 0] = phi / (2 * pi)
            new_coords[j, 0] = if(new_coords[j, 0] < -eps, new_coords[j, 0] + 1, new_coords[j, 0])
            <float64> _nc_abs = fabs(new_coords[j, 0])
            new_coords[j, 0] = if(_nc_abs < eps and Y < 0, 1, new_coords[j, 0])
            new_coords[j, 1] = theta / (2 * pi)
            new_coords[j, 1] = if(new_coords[j, 1] < -eps, new_coords[j, 1] + 1, new_coords[j, 1])
            _nc_abs = fabs(new_coords[j, 1])
            new_coords[j, 1] = if(_nc_abs < eps and Z < 0, 1, new_coords[j, 1])
            new_coords[j, 0] = new_coords[j, 0] * Lx[0]
            new_coords[j, 1] = new_coords[j, 1] * Ly[0]
        end
        """
        cLx = Constant(1)
        cLy = Constant(1)

        par_loop((domain, instructions), dx,
                 {"new_coords": (new_coordinates, WRITE),
                  "old_coords": (old_coordinates, READ),
                  "Lx": (cLx, READ),
                  "Ly": (cLy, READ)},
                  is_loopy_kernel=True)

        return Mesh(new_coordinates)

    @staticmethod
    def snap(mesh, N, L=1):
        coords = mesh.coordinates.dat.data
        coords[...] = np.round((N/L)*coords)*(L/N)
