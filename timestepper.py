from firedrake import *
import ufl
import numpy as np

from abc import ABCMeta, abstractmethod


class System(metaclass=ABCMeta):
    r'''Abstract base class describing a system
    equation of motion :math:`M q_t = N(q) + Lq` where :math:`N` is a
    non-linear operator and :math:`L` is a linear operator. :math:`M` is a
    finite element mass matrix.

    The class provides methods for carrying out the following operations:
      * create 1-form for applying the mass matrix to a state
        i.e. return the form representing :math:`Mq`
      * create 1-form for applying the linear operator to a state
        i.e. return the form representing :math:`Lq`
      * create 1-form for applying the non-linear operator to a state
        i.e. return the form representing :math:`N(q)`
      * create a solver for the equation :math:`(M-\\alpha L)q = r` for a
        given choice of :math:`\alpha`
      * create a mass solver for the equation :math:`Mq = r`
    '''
    def __init__(self):
        pass

    @abstractmethod
    def V(self):
        '''Return state function space'''
        pass

    @abstractmethod
    def resN(self, q):
        r'''Apply the non-linear part :math:`N` of the problem to a particular
        state and return :math:`N(q)` as a 1-form.

        :arg q: state to apply to
        '''
        pass

    @abstractmethod
    def resL(self, q):
        r'''Apply the linear part :math:`L` of the problem to a particular
        state and return :math:`Lq` as a 1-form.

        :arg q: state to apply to
        '''
        pass

    @abstractmethod
    def resM(self, q):
        r'''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        pass

    @abstractmethod
    def linear_solver(self, r, q, alpha):
        r'''Solver for the linear system :math:`(M-\\alpha L)q=r` for a given
        right hand side :math:`r` and solution :math:`q`.

        :arg r: residual :math:`r`
        :arg q: solution
        :arg alpha: scaling parameter
        '''
        pass

    @abstractmethod
    def mass_solver(self, r, q):
        r'''Solver for the linear system :math:`Mq=r` for a given
        right hand side :math:`r` and the solution :math:`q`.

        :arg r: right hand side :math:`r`
        :arg q: solution
        '''
        pass


class TimeStepperMonitor(metaclass=ABCMeta):
    '''Abstract base class for monitor that can be used
    in the timestepper class'''
    def __init__(self):
        pass

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    @abstractmethod
    def __call__(self, t, q):
        pass


class TimeStepperMonitorDummy(TimeStepperMonitor):
    def __init__(self):
        pass

    def __call__(self, t, q):
        pass


class SaveToDiskMonitor(TimeStepperMonitor):
    r'''Timestepper monitor which saves the fields to disk at every
    timestep

    :arg filename: Name of file to write
    :arg field_idx: Index of field to be saved
    '''
    def __init__(self, filename, field_idx=None, field_names=[],interval=1,comm=None):
        self._filename = filename
        if not isinstance(field_idx, list) and field_idx is not None:
            field_idx = [field_idx]
        self._field_idx = field_idx
        if not isinstance(field_names, list):
            field_names = [field_names]
        self._field_names = field_names
        self._interval = interval
        self._k_cur = 0
        self._comm = comm

    def __enter__(self):
        self._k_cur = 0
        self._outfile = File(self._filename)

    def __call__(self, t, q):
        if (self._k_cur == 0):
            if not (self._comm is None):
                if (self._comm.Get_rank() == 0):
                    print ('t = '+('%12.3f' % t),flush=True)

            if self._field_idx is None:
                f = q
            else:
                f = [q.split()[i] for i in self._field_idx]
                n = 0
                for word in self._field_names:
                    f[n].rename(word)
                    n += 1
            self._outfile.write(*f, time=t)
        self._k_cur += 1
        if (self._k_cur > self._interval):
            self._k_cur = 0



class TimeStepper(metaclass=ABCMeta):
    r'''Solve the system

     .. math ::
        \\frac{\\partial q}{\\partial t} = N(q) + Lq

    using a particular discrete timestepping method.

    :arg system: instance of a system class, describing :math:`N`
                 and :math:`L`.
    :arg dt: time step size
    :arg monitor: monitor object which is called in each timestep
    '''
    def __init__(self, system, dt, monitor=None):
        '''
        :arg dt: time step size
        '''
        self._system = system
        self._system.reset_ksp_monitors()
        self._dt = dt
        self._monitor = monitor

    @abstractmethod
    def integrate(self, q0, tfinal):
        r'''Step the initial solution :math:`q_0` to final time :math:`T`
        time steps.

        :arg q0: initial solution :math:`q_0`
        :arg tfinal: final time :math:`T`
        '''
        pass


class TimeStepperTheta(TimeStepper):
    r'''
    Timestepper based on the :math:`\\theta` method.

     .. warning::
      Only works for the non-hybridized system!

     .. math ::
        (1-\\mu \\Delta t L) q^{(n+1)} = q^{(n)}
        + \\Delta t (1-\\mu)L(q^{(n)}) + \Delta t N(q^{n})

    :arg system: instance of a system class, describing :math:`N`
                 and :math:`L`.
    :arg dt: time step size
    :arg monitor: monitor object which is called in each timestep
    '''
    def __init__(self, system, dt, mu, monitor=None):
        super(TimeStepperTheta, self).__init__(system, dt, monitor)
        self._mu = mu
        self.label = "Theta_mu" + ('%-6.3f' % mu).strip()

    def integrate(self, q0, tfinal):
        t = 0
        qn = Function(self._system.V())
        qn.assign(q0)
        qn1 = Function(self._system.V())
        Mq = self._system.resM(qn)
        resN = self._system.resN(qn)
        resL = self._system.resL(qn)
        Mr = Mq + (1.0-self._mu)*self._dt*resL + self._dt*resN
        alpha = self._mu*self._dt
        epsilon = 1.E-12
        if self._mu < epsilon:
            solver = self._system.mass_solver(Mr, qn1)
        else:
            solver = self._system.linear_solver(Mr, qn1, alpha)

        if not (self._monitor is None):
            self._monitor(t, qn)
        while t < tfinal:
            solver.solve()
            qn.assign(qn1)
            t += self._dt
            if not (self._monitor is None):
                self._monitor(t, qn)
        return qn


class TimeStepperIMEX(TimeStepper):
    r'''
    Timestepper based on implicit/explicit IMEX methods. Those
    methods are defined by the :math:`s\times s` matrices :math:`a`,
    :math:`\\tilde{a}` and the vectors :math:`b`, :math:`\\tilde{b}`. Given
    :math:`q^{(n)}`, the state :math:`q^{(n+1)}` at the next timestep is
    defined by.

      .. math::
        \\begin{aligned}
        MQ^{(i)} &= Mq^{(n)} + \\Delta t \\sum_{j=1}^{i-1} a_{ij} N(Q^{(j)})
                 +  \\Delta t \\sum_{j=1}^{i}\\tilde{a}_{ij} L(Q^{(j)})\\\\
        Mq^{(n+1)} &= Mq^{(n)} + \Delta t \\sum_{i=1}^{s} \\left(b_i N(Q^{(i)})
                                                    + \\tilde{b}_i L(Q^{(i)})\\right)
        \\end{aligned}

    :arg system: instance of a system class, describing :math:`N`
                 and :math:`L`.
    :arg dt: time step size
    :arg monitor: monitor object which is called in each timestep
    '''

    def __init__(self, system, dt, monitor=None):
        super(TimeStepperIMEX, self).__init__(system, dt, monitor)
        self._a = None
        self._atilde = None
        self._b = None
        self._btilde = None
        self.label = "IMEX"

    def integrate(self, q0, tfinal):
        r'''
        Implements the following iteration to update the state
        :math:`q^{(n)} \\mapsto q^{(n+1)}`

        :math:`\\hat{q}^{(n+1)} = Mq^{(n)}`

        do :math:`i = 1, s`

        :math:`\\hat{Q}^{(i)} = Mq^{(n)}`

        do :math:`j = 1, i-1`

        :math:`\\hat{Q}^{(i)} += \Delta t (a_{ij} N_j + \\tilde{a}_{ij} L_j)`

        end do

        if (:math:`\\tilde{a}_{ii} = 0`):

        :math:`Q^{(i)} = M^{-1} \\hat{Q}^{(i)}`

        else:

        :math:`Q^{(i)} = (M - \\tilde{a}_{ii} L)^{-1} \\hat{Q}^{(i)}`,

        :math:`N_i = N(Q^{(i)})`,

        :math:`L_i = L(Q^{(i)})`

        :math:`\\hat{q}^{(n+1)} += \\Delta t (b_i N_i + \\tilde{b}_i L_i)`

        end do

        :math:`q^{(n+1)} = M^{-1} \\hat{q}^{(n+1)}`
        '''

        if (self._a is None):
            raise Exception("Timestepping matrices a not defined. Use derived class.")
        t = 0
        qn = Function(self._system.V())
        qn1 = Function(self._system.V())
        Mq = self._system.resM(qn)
        Q = []
        NQ = []
        LQ = []
        Qhat = []
        solvers = []
        for i in range(self._stages):
            Q.append(Function(self._system.V()))
            Qhat.append(self._system.resM(qn))
        for i in range(self._stages):
            NQ.append(self._system.resN(Q[i]))
            LQ.append(self._system.resL(Q[i]))

        atilde_nonzero = not isinstance(self._atilde, ufl.classes.Zero)
        qhatn1 = self._system.resM(qn)
        for i in range(self._stages):
            for j in range(i):
                Qhat[i] += self._dt*self._a[i][j]*NQ[j]
                if (atilde_nonzero):
                    if (not (self._atilde[i][j] == zero())):
                        Qhat[i] += self._dt*self._atilde[i][j]*LQ[j]
            qhatn1 += self._dt*self._b[i]*NQ[i]
            qhatn1 += self._dt*self._btilde[i]*LQ[i]
        for i in range(self._stages):
            if (atilde_nonzero):
                atilde_diag = self._atilde[i][i]
                if (atilde_diag is zero()):
                    solvers.append(self._system.mass_solver(Qhat[i], Q[i]))
                else:
                    solvers.append(self._system.linear_solver(Qhat[i], Q[i],
                                                              self._dt*atilde_diag))
            else:
                solvers.append(self._system.mass_solver(Qhat[i], Q[i]))
        solvers.append(self._system.mass_solver(qhatn1, qn1))

        qn.assign(q0)

        if not (self._monitor is None):
            self._monitor(t, qn)
        while t < tfinal:
            for i, solver in enumerate(solvers):
                solver.solve()

            qn.assign(qn1)
            t += self._dt
            if not (self._monitor is None):
                self._monitor(t, qn)
        return qn


class TimeStepperIMEXTheta(TimeStepperIMEX):
    '''IMEX version of theta method'''
    def __init__(self, system, dt, mu, monitor=None):
        super(TimeStepperIMEXTheta, self).__init__(system, dt, monitor)
        self._stages = 2
        self._a = as_tensor([[0, 0],
                             [1, 0]])
        self._atilde = as_tensor([[0,  0],
                                  [1-mu, mu]])
        self._b = as_tensor([1, 0])
        self._btilde = as_tensor([1-mu, mu])
        self.label = "IMEXTheta_mu"+('%-6.3f' % mu).strip()


class TimeStepperIMEXThreeStage(TimeStepperIMEX):
    '''Three stage IMEX method'''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperIMEXThreeStage, self).__init__(system, dt, monitor)
        self._stages = 3
        self._a = as_tensor([[0, 0, 0],
                             [0, 0, 0],
                             [0, 1, 0]])
        self._atilde = as_tensor([[ 0.5,   0,   0],
                                  [-0.5, 0.5,   0],
                                  [   0, 0.5, 0.5]])
        self._b = as_tensor([0, 0.5, 0.5])
        self._btilde = as_tensor([0, 0.5, 0.5])
        self.label = "IMEXSSP2(3,2,2)"


class TimeStepperIMEXARS2_2_3_2(TimeStepperIMEX):
    '''Three stage IMEX method'''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperIMEXARS2_2_3_2, self).__init__(system, dt, monitor)
        gamma = 1.-1./np.sqrt(2.)
        delta = -2./3.*np.sqrt(2)
        self._stages = 3
        self._a = as_tensor([[0    ,       0, 0],
                             [gamma,       0, 0],
                             [delta, 1-delta, 0]])
        self._atilde = as_tensor([[0,       0,     0],
                                  [0,   gamma,     0],
                                  [0, 1-gamma, gamma]])
        self._b = as_tensor([0, 1-gamma, gamma])
        self._btilde = as_tensor([0, 1-gamma, gamma])
        self.label = "IMEXARS2(2,3,2)"


class TimeStepperIMEXARS3_4_4_3(TimeStepperIMEX):
    '''Three stage IMEX method'''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperIMEXARS3_4_4_3, self).__init__(system, dt, monitor)
        self._stages = 5
        self._a = as_tensor([[      0,      0,     0,      0, 0],
                             [  1./2.,      0,     0,      0, 0],
                             [11./18.,  1./18,     0,      0, 0],
                             [  5./6., -5./6., 1./2.,      0, 0],
                             [  1./4.,  7./4., 3./4., -7./4., 0]])
        self._atilde = as_tensor([[0,      0,      0,     0,     0],
                                  [0,  1./2.,      0,     0,     0],
                                  [0,  1./6.,  1./2.,     0,     0],
                                  [0, -1./2.,  1./2., 1./2.,     0],
                                  [0,  3./2., -3./2., 1./2., 1./2.]])
        self._b = as_tensor([1./4., 7./4., 3./4., -7./4., 0])
        self._btilde = as_tensor([0, 3./2., -3./2., 1./2., 1./2.])
        self.label = "IMEXARS3(4,4,3)"

class TimeStepperRKDG(TimeStepper):
    r'''
    Timestepper based on fully explicit RKDG methods.

    See Those
    methods are defined by the lower-triangular
    :math:`s\times s` matrix :math:`a`, and the vector :math:`b`. Given
    :math:`q^{(n)}`, the state :math:`q^{(n+1)}` at the next timestep is
    defined by:

    Q^{(1)} = q^{(n)}
    MQ^{(i)} = Mq^{(n)} + \\Delta t \\sum_{j=1}^{i-1} a_{ij} (L Q^{(j)} + N(Q^{(j)})) for i = 2,...,s
    Mq^{(n+1)} = Mq^{(n)} + \\Delta t \\sum_{i=1}^{s} b_{i} (L Q^{(j)} + N(Q^{(j)}))

    An s-stage RKDG method requires s mass solves and s function evaluations.

    References:

    * Cockburn, B. and Shu, C.W., 1989. TVB Runge-Kutta local projection
      discontinuous Galerkin finite element method for conservation laws. II.
      General framework. Mathematics of computation, 52(186), pp.411-435.

    * Cockburn, B. and Shu, C.W., 1998. The Runge–Kutta discontinuous Galerkin
      method for conservation laws V: multidimensional systems. Journal of
      Computational Physics, 141(2), pp.199-224.

    :arg system: instance of a system class, describing :math:`N`
                 and :math:`L`.
    :arg dt: time step size
    :arg monitor: monitor object which is called in each timestep
    '''

    def __init__(self, system, dt, monitor=None):
        super(TimeStepperRKDG, self).__init__(system, dt, monitor)
        self._a = None
        self._b = None
        self.label = "RKDG"

    def integrate(self, q0, tfinal):
        r'''
        Implements the iteration to update the state
        :math:`q^{(n)} \\mapsto q^{(n+1)}` Note that it requires exactly
        s mass solves and s evaluations of the non-linear function.
        '''

        if (self._a is None):
            raise Exception("Timestepping matrices a not defined. Use derived class.")
        t = 0
        qn = Function(self._system.V())
        qn1 = Function(self._system.V())
        Mq = self._system.resM(qn)
        Q = [qn]
        NLQ = []
        Qhat = []
        solvers = []
        for i in range(self._stages-1):
            Q.append(Function(self._system.V()))
            Qhat.append(self._system.resM(qn))
        qhatn1 = self._system.resM(qn)
        for i in range(self._stages):
            NLQtmp = self._system.resN(Q[i])+self._system.resL(Q[i])
            qhatn1 += self._dt*self._b[i]*NLQtmp
            NLQ.append(NLQtmp)

        for i in range(self._stages-1):
            for j in range(i+1):
                Qhat[i] += self._dt*self._a[i+1][j]*NLQ[j]
        for i in range(self._stages-1):
            solvers.append(self._system.mass_solver(Qhat[i], Q[i+1]))
        solvers.append(self._system.mass_solver(qhatn1, qn1))

        qn.assign(q0)

        if not (self._monitor is None):
            self._monitor(t, qn)
        while t < tfinal:
            for i, solver in enumerate(solvers):
                solver.solve()

            qn.assign(qn1)
            t += self._dt
            if not (self._monitor is None):
                self._monitor(t, qn)
        return qn

class TimeStepperRKDG2(TimeStepperRKDG):
    '''Two stage RKDG method

    This is the same as Heun's method.
    '''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperRKDG2, self).__init__(system, dt, monitor)
        self._stages = 2
        self._a = as_tensor([[0, 0],
                             [1, 0]])
        self._b = as_tensor([0.5, 0.5])
        self.label = "Heun"

class TimeStepperRKDG3(TimeStepperRKDG):
    '''Three stage RKDG method

    This is the Third-order Strong Stability Preserving Runge-Kutta (SSPRK3)
    method.
    '''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperRKDG3, self).__init__(system, dt, monitor)
        self._stages = 3
        self._a = as_tensor([[0, 0, 0],
                             [1, 0, 0],
                             [0.25, 0.25, 0]])
        self._b = as_tensor([1./6., 1./6., 2./3.])
        self.label = "SSPRK3"

class TimeStepperIMEXRKDG2(TimeStepperIMEX):
    '''Two stage RKDG method, implemented as an IMEX timestepper.

    This is the same as Heun's method. Should give the same results as
    TimeStepperRKDG2, and can be used for debugging.
    '''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperRKDG2IMEX, self).__init__(system, dt, monitor)
        self._stages = 2
        self._a = as_tensor([[0, 0, 0],
                             [1, 0, 0]])
        self._b = as_tensor([0.5, 0.5, 0])
        self._atilde = self._a
        self._btilde = self._b
        self.label = "IMEXHeun"

class TimeStepperIMEXRKDG3(TimeStepperIMEX):
    '''Three stage RKDG method, implemented as an IMEX timestepper

    This is the Third-order Strong Stability Preserving Runge-Kutta (SSPRK3)
    method. Should give the same results as TimeStepperRKDG3,
    and can be used for debugging.
    '''
    def __init__(self, system, dt, monitor=None):
        super(TimeStepperRKDG3IMEX, self).__init__(system, dt, monitor)
        self._stages = 4
        self._a = as_tensor([[0, 0, 0, 0],
                             [1, 0, 0, 0],
                             [0.25, 0.25, 0, 0],
                             [1./6., 1./6., 2./3.,0]])
        self._b = as_tensor([1./6., 1./6., 2./3., 0])
        self._atilde = self._a
        self._btilde = self._b
        self.label = "IMEXSSPRK3"
