import math
import numpy as np
from firedrake import *

'''Exact stationary solution of the (non-)linear shallow water equations'''

class BalancedVortex(object):
    r'''Class for calculating the functions :math:`\phi_S(r)`, :math:`\phi_B(r)` and
    :math:`u(r)` which solve the stationary (non-)linear shallow water
    equations with a Coriolis term:

    .. math::
      \nabla\cdot\vec{u} = 0\\
      \nabla\cdot\left(\frac{\vec{u}\otimes\vec{u}}{\phi_S + \phi_B}+\frac{1}{2}\phi^2 + \phi_B\phi_S\right) + f\hat{z}\times \vec{u} - \phi_S\nabla{\phi_B}= 0

    where :math:`\phi = \phi_B + \phi_s` is the total water column height.
    The solutions are of the form

    .. math::
      \phi_S(r) = \begin{cases}
          - \Delta \Phi & \text{for $r<r_-$}\\
          - \Delta\Phi/2\left(1+\tanh\left[\sigma/(r-r_-)+\sigma/(r-r_+)\right]\right) & \text{for $r_-\le r\le r_+$\\
          0 & \text{for $r_+<r$}
      \end{cases}
      
    .. math::
      \phi_B(r) = \begin{cases}
          1 & \text{for $r<r_-$}\\
          1 - \frac{1}{2}\Delta\Phi_B\exp^{\frac{1}{r - r_+} + \frac{4}{r_+ - r_-} - \frac{1}{r - r_-}} & \text{for $r_-\le r\le r_+$\\
          1 & \text{for $r_+<r$}
      \end{cases}
    
    with :math:`\vec{u}(r)=u(r)\vec{e}_{\theta}` and
    
    .. math::
      u(r) = \frac{f}{2c_g}r(\phi_S + \phi_B) \left(-1+\sqrt{1+4\frac{c_g^2}{f^2}}\frac{\phi'}{r}\right)

    in the non-linear case and
    
    .. math::
      u(r) = \frac{c_g}{f} (\phi_S + \phi_B) \phi'

    in the linear case

    :arg dphi: Parameter :math:`\Delta\phi` -- depth of vortex
    :arg f_over_cg: Non-dimensionaled ratio of the Coriolis parameter :math:`f` and the gravity wave speed :math:`c_g`
    :arg non_linear: Consider non-linear case?
    :arg r_m: Lower radius bound :math:`r_-`
    :arg r_p: Upper radius bound :math:`r_+`
    :arg sigma: Steepness parameter :math:`\sigma`
    :arg bathymetry: Boolean turning bathymetry on or off (assumed constant 1.0 if False)
    :arg dphi_B: Parameter :math:`\Delta\phi` -- height of bathymetry bump
    '''
    def __init__(self,dphi,f_over_cg,
                 non_linear=True,
                 r_m=0.05,r_p=0.45,sigma=0.25,
                 bathymetry=False, dphi_B=-0.1):
        self._dphi = dphi
        self._f_over_cg = f_over_cg
        self._non_linear=non_linear
        self._r_m = r_m
        self._r_p = r_p
        self._sigma = sigma
        self._bathymetry = bathymetry
        self._dphi_B = dphi_B

    def phi(self,r):
        r'''Function :math:`\phi(r)`

        :arg r: Radius :math:`r`
        '''
        if (r <= self._r_m):
            height = 1.0-self._dphi
        elif (r >= self._r_p):
            height = 1.0
        else:
            # Hyperbolic angle
            hangle = self._sigma/(r - self._r_m)
            hangle += self._sigma/(r - self._r_p)
            slope = self._dphi*(1.0 + np.tanh(hangle))
            height = 1.0 - 0.5*slope
        return height
    
    def u(self,r):
        r'''Function :math:`u(r)`

        :arg r: Radius :math:`r`
        '''
        if ( (r <= self._r_m) or (self._r_p <= r)):
            momentum = 0.0
        else:
            dphi_dr = 0.5*self._sigma*self._dphi
            dphi_dr *= (1.0/(r - self._r_m)**2 + 1.0/(r - self._r_p)**2)
            dphi_dr *= (1.0 - np.tanh(self._sigma/(r - self._r_m) + self._sigma/(r - self._r_p))**2)
            if (self._non_linear):
                momentum = 0.5*self._f_over_cg*r*(self.phi(r) + self.bath(r))
                momentum *= (-1.0 + np.sqrt(1.0 + 4.0/(self._f_over_cg)**2*dphi_dr/r))
            else:
                momentum = 1.0/self._f_over_cg*self.bath(r)*dphi_dr
        return momentum
       
    def bath(self, r):
        r'''Function :math:`\phi_B(r)`

        :arg r: Radius :math:`r`
        '''
        if ((self._bathymetry==False) or (r <= self._r_m) or (self._r_p <= r)):
            height = 1.0
        else:
            exponent =  1.0/(r - self._r_p)
            exponent += 4.0/(self._r_p - self._r_m)
            exponent -= 1.0/(r - self._r_m)
            height = 1.0 + self._dphi_B*np.exp(exponent)
        return height
    
    def phis_ufl(self,mesh):
        r'''Return the height perturbation `\phi_s(x,y)` as an UFL expression
        
        :arg mesh: Mesh from which to extract the coordinates
        '''
        x,y = SpatialCoordinate(mesh)
        x -= 0.5
        y -= 0.5
        r = sqrt(x**2+y**2)
        return conditional(r<=self._r_m,-self._dphi,
                           conditional(r>=self._r_p,0.0,
                                       -0.5*self._dphi*(1.0+tanh(self._sigma/(r-self._r_m)+self._sigma/(r-self._r_p)))))

    def u_ufl(self,mesh):
        r'''Return the momentum `(u_x(x,y),u_y(x,y))` as an UFL expression
        
        :arg mesh: Mesh from which to extract the coordinates
        '''
        x,y = SpatialCoordinate(mesh)
        x -= 0.5
        y -= 0.5
        r = sqrt(x**2+y**2)
        potential = self.phis_ufl(mesh) + self.bath_ufl(mesh)
        dphi_dr = 0.5*self._sigma*self._dphi*(1.0/(r-self._r_m)**2+1.0/(r-self._r_p)**2)
        dphi_dr*= (1.0-tanh(self._sigma/(r-self._r_m)+self._sigma/(r-self._r_p))**2)
        if (self._non_linear):
            u_r_expr = 0.5*self._f_over_cg*potential # No r as this is provided by the coordinate!
            u_r_expr*= (-1.0+sqrt(1+4.0/(self._f_over_cg)**2*dphi_dr/r))
        else:
            u_r_expr = (1.0/self._f_over_cg)*self.bath_ufl(mesh)*(dphi_dr/r)
        return conditional(Or(r<=self._r_m,self._r_p<=r),as_vector((0.0,0.0)),
                           as_vector((-y*u_r_expr,x*u_r_expr)))
    
    def bath_ufl(self, mesh):
        r'''Return the bathymetry `\phi_s(x,y)` as an UFL expression
        
        :arg mesh: Mesh from which to extract the coordinates
        '''
        if self._bathymetry==False:
            height = Constant(1.0)
        else:
            x,y = SpatialCoordinate(mesh)
            x -= 0.5
            y -= 0.5
            r = sqrt(x**2+y**2)
            
            exponent =  1/(r - self._r_p)
            exponent += 4/(self._r_p - self._r_m)
            exponent -= 1/(r - self._r_m)
            bump = 1.0 + self._dphi_B*exp(exponent)
            height = conditional(r<=self._r_m, 1.0,
                           conditional(r>=self._r_p, 1.0,
                                       bump))
        return height

            
if (__name__ == '__main__'):
    from matplotlib import pyplot as plt
    from matplotlib.patches import Polygon
    # Plot the functions phi(r), u(r) and v(r) (the non-dimensionalised velocity)
    X = np.arange(0.0,0.5,0.001)
    dphi = 0.1
    # Reference length: radius of Earth = 6.370E6 [m]
    Rearth = 6.37122E6
    Xref = Rearth
    # Gravitational acceleration g_grav = 9.81 [ms^{-2}]
    g_grav = 9.80616
    # Depth of fluid = 2.0E3 [m]
    H = 2.0E3
    # Reference time = 1 day = 24*3600 [s]
    day = 24*3600
    Tref = day
    cg = Tref / Xref * math.sqrt(g_grav*H)
    # angular frequency of Earth rotation
    f_coriolis = 4.0*math.pi/day*Tref
    f_over_cg = f_coriolis/cg

    print ("non-dimensionalised c_g     = ",cg)
    print ("non-dimensionalised f       = ",f_coriolis)
    print ("non-dimensionalised f/c_g   = ",f_over_cg)
    plt.clf()
    for i, non_linear in enumerate((True,)):
        ax = plt.subplot(2, 1, (i+1))
        balanced_vortex = BalancedVortex(dphi,f_over_cg,non_linear,bathymetry=True)
        vec_phi = np.vectorize(balanced_vortex.phi)
        vec_u = np.vectorize(balanced_vortex.u)
        vec_bath = np.vectorize(balanced_vortex.bath)
        plt.plot(X,vec_phi(X)-1,
                 linewidth=2,
                 color='b',
                 label=r'potential perturbation $\phi_S(r)$')
        plt.plot(X,vec_u(X),
                 linewidth=2,
                 color='r',
                 label=r'momentum $u(r)$')
        hcoords = [*zip(X, vec_phi(X) - 1), *zip(X[::-1], -vec_bath(X)[::-1])]
        hpoly = Polygon(hcoords, fc='blue', alpha=0.5)
        ax.add_patch(hpoly)
        #~ plt.plot(X,vec_u(X)/vec_phi(X),
                 #~ linewidth=2,
                 #~ color='r',
                 #~ linestyle='--', 
                 #~ label=r'velocity $v(r)$')
        plt.plot(X, - vec_bath(X),
                 linewidth=2,
                 color='g', 
                 label=r'bathymetry $-\phi_B(r)$')
        bcoords = [(0, -1.1), *zip(X, -vec_bath(X)), (0.5, -1.1)]
        bpoly = Polygon(bcoords, fc='g', alpha=0.5, ec='g', hatch='XX')
        ax.add_patch(bpoly)
        
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width*0.8, box.height*0.8])
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax = plt.gca()
        ax.set_xlabel('Radius $r$')
        #~ ax.set_title('Non-linear =' + str(non_linear))
    plt.savefig('balanced_vortex.pdf',bbox_inches='tight')
