import sys
from firedrake import *
from firedrake.utils import cached_property
from firedrake import NonNestedHierarchy
from pyop2.profiling import timed_function
import math
from ksp_monitor import *
from timestepper import *
from solver_wrapper import WrappedSolver
from transfer_kernels import prolongation_matrix

class ShallowWaterParam(object):
    '''Class for holding constant parameters about the Earth
    Allows retrieving parameters without instantiating rest of
    shallow water object

    :arg williamson: Williamson et al test class (used to extract parameters)
    '''
    def __init__(self,williamson=None):
        # Radius of Earth = 6.37122E6 [m]
        self._Rearth = 6.37122E6
        # Gravitational acceleration g_grav = 9.80616 [ms^{-2}]
        self._g_grav = 9.80616

        if (williamson is None):
            # Depth of fluid = 2.0E3 [m]
            self._H = 2.0E3
        else:
            self._H = williamson.h0

        # Reference time = 1 day = 24*3600 [s]
        day = 24*3600
        self._day = day
        self._Tref = day
        self._cg = self._Tref / self._Rearth * math.sqrt(self._g_grav*self._H)

        # angular frequency of Earth rotation
        self._Omega = (2.0*math.pi/day)*self._Tref

    @property
    def cg(self):
        return self._cg

    @property
    def omega(self):
        return self._Omega

class ShallowWater(ShallowWaterParam, System):
    '''Finite Element linear shallow water solver with Coriolis force on a unit
    sphere or a biperiodic unit square.

    :arg geometry: Mesh geometry, can be 'spherical' or 'flat'
    :arg degree: Polynomial degree of finite element spaces
    :arg mesh: Mesh object to use. If this is not passed, then a mesh in
         created in the constructor
    :arg bathymetry: UFL expression for bathymetry. If this is None, a flat
         bathymetry (:math:`\Phi_b=1` is used).
    :arg coriolis: Include Coriolis force
    :arg nonlinear: Include non-linear terms in the equations
    :arg output_filename: Name of output file (None for no output)
    :arg solver_rtol: Relative tolerance to be used in linear solve
    :arg ksp_verbose: KSP verbosity level
    :arg williamson: Williamson et al test class (used to extract parameters)
    '''
    def __init__(self,
                 geometry,
                 degree,
                 mesh,
                 bathymetry=None,
                 coriolis=True,
                 nonlinear=False,
                 output_filename=None,
                 solver_rtol=1.0E-8,
                 ksp_verbose=0,
                 solver_configuration={'solver' : 'direct',
                                       'coarse_solver' : None,
                                       'coarse_space' : None},
                 williamson=None):
        super(ShallowWater, self).__init__(williamson)
        # Check geometry for validity
        assert ((geometry == 'spherical') or (geometry == 'flat'))
        self._geometry = geometry
        self._degree = degree
        self._mesh = mesh
        self._nonlinear = nonlinear

        # Set bathymetry
        self._constant_bathymetry = (bathymetry is None)
        if self._constant_bathymetry:
            self._bath = Constant(1.0, domain=self._mesh)
        else:
            self._bath = bathymetry

        self._coriolis = coriolis

        # Communicator
        self._comm = self._mesh.comm
        # Construct function spaces
        self._V_phi = FunctionSpace(self._mesh, "DG", self._degree-1)
        if mesh.ufl_cell().cellname()=='quadrilateral':
            self._V_u = FunctionSpace(self._mesh, "RTCF", self._degree)
        else:
            self._V_u = FunctionSpace(self._mesh, "RT", self._degree)
        print (self._V_u.cell_node_map())
        self._V_q = self._V_u * self._V_phi
        self._w, self._psi = TestFunctions(self._V_q)
        # Function for the current state
        self.q = Function(self._V_q, name="State")
        # Function for the state at the previous time-step
        self.qold = Function(self._V_q, name="Old state")
        # Name of output file
        self._output_filename = output_filename
        # Solver tolerance
        self._solver_rtol = solver_rtol
        # higher order qudrature rules
        self._dS = dS(domain=self._mesh)
        self._dx = dx(domain=self._mesh)
        self._w, self._psi = TestFunctions(self._V_q)
        self._ksp_verbose = ksp_verbose
        self.reset_ksp_monitors()
        self.solver = solver_configuration['solver']
        self.coarse_solver = solver_configuration['coarse_solver']
        self.coarse_space = solver_configuration['coarse_space']

    def reset_ksp_monitors(self):
        '''Reset at beginning of new integration'''
        self._ksp_monitors = []

    @property
    def V_q(self):
        '''Mixed function space :math:`V_q = V_h^k\\otimes (V_h^k)^2`'''
        return self._V_q

    def V(self):
        '''Return state function space'''
        return self.V_q

    def _resL(self, u, phi):

        psi = self._psi
        w = self._w

        return self._cg*(phi*div(self._bath*w)-psi*div(u))*self._dx

    def resL(self, q):
        '''Apply the linear part :math:`L` of the problem to a particular
        state and return :math:`Lq` as a 1-form.

        :arg q: state to apply to
        '''
        u, phi = q.split()
        return self._resL(u,phi)

    def _resN(self, u, phi):
        '''Calculate the slow part of the operator, i.e.
        :math:`N(u,\\phi)-L(u,\\phi)` for given momentum and potential
        functions :math:`u` and :math:`\\phi'. This is an internal method
        which gets called by resN().

        :math u: Momentum function :math:`u`
        :math phi: Potential function :math:`\\phi`
        '''

        psi = self._psi
        w = self._w

        phiB = self._bath

        if (self._coriolis):
            if (self._geometry == 'spherical'):
                X = SpatialCoordinate(self._mesh)
                x, y, z = X
                r_hat = X/sqrt(x*x+y*y+z*z)
                coriolis_integral = z*dot(self._w, cross(r_hat, u))*self._dx
            else:
                # f-plane approximation at 90 degrees of latitude : math.sin(math.pi/2)==1
                coriolis_integral = dot(self._w, perp(u))*self._dx

            coriolis_integral = -2.0*self._Omega*coriolis_integral
        else:
            coriolis_integral = 0

        if (self._nonlinear):
            # 2x2 or 3x3 matrix which arises in the non-linear term
            if (self._geometry == 'flat'):
                Fu_NL = as_matrix([ [ u[0]**2/(phi+phiB) + 0.5*phi**2,
                                      u[0]*u[1]/(phi+phiB) ],
                                    [ u[0]*u[1]/(phi+phiB),
                                      u[1]**2/(phi+phiB) + 0.5*phi**2] ])

            else:
                Fu_NL = as_matrix([ [ u[0]**2/(phi + phiB) + 0.5*phi**2,
                                      u[0]*u[1]/(phi+phiB),
                                      u[0]*u[2]/(phi+phiB) ],
                                    [ u[0]*u[1]/(phi+phiB),
                                      u[1]**2/(phi+phiB) + 0.5*phi**2,
                                      u[2]*u[1]/(phi+phiB) ],
                                    [ u[0]*u[2]/(phi+phiB),
                                      u[2]*u[1]/(phi+phiB),
                                      u[2]**2/(phi+phiB) + 0.5*phi**2] ])
            nonlinear_integral = self._cg*inner(Fu_NL,grad(w))*self._dx
        else:
            nonlinear_integral = 0

        return  nonlinear_integral + coriolis_integral

    def resN(self, q):
        '''Calculate the slow part of the operator, i.e.
        :math:`N(q)-L(q)` for a given state :math:`q=(u,\\phi).`
        This is an outward facing method which gets called for example by the
        timestpper algorithm. It calls the internal method _resN().

        :math q: State function :math:`q=(u,\\phi)`
        '''
        u, phi = q.split()
        return self._resN(u, phi)

    def _resM(self, u, phi):
        '''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        return (self._psi*phi + inner(u, self._w))*self._dx

    def resM(self, q):
        '''Apply the mass matrix :math:`M` of the problem to a particular
        state and return :math:`Mq` as a 1-form.

        :arg q: state to apply to
        '''
        u, phi = q.split()
        return self._resM(u, phi)

    def linear_solver(self, r, q, alpha):
        '''Solver for the linear system :math:`(M-\\alpha L)q=r` for a given
        right hand side :math:`r` and solution :math:`q`.

        :arg r: residual :math:`r`
        :arg q: solution
        :arg alpha: scaling parameter
        '''
        # Set up default appctx
        appctx = {}

        # Set up solver
        if (self.solver == 'direct'):
            solver_param = {'mat_type': 'aij',
                            'ksp_type': 'preonly',
                            'pc_type': 'lu',
                            'pc_factor_mat_solver_type': 'mumps'}
        elif (self.solver == 'pressure_multigrid'):
            solver_param = {'ksp_type': 'gmres',
                            'ksp_rtol': self._solver_rtol,
                            'pc_type': 'fieldsplit',
                            'pc_fieldsplit_type': 'schur',
                            'pc_fieldsplit_schur_fact_type': 'FULL',
                            'pc_fieldsplit_schur_precondition': 'selfp',
                            'fieldsplit_0': {'ksp_type': 'preonly',
                                            'pc_type': 'bjacobi',
                                            'sub_pc_type': 'ilu'},
                            'fieldsplit_1': {'ksp_type': 'preonly',
                                            'pc_type': 'hypre',
                                            'pc_mg_log': None,
                                            'mg_levels': {'ksp_type': 'richardson',
                                                          'ksp_richardson_scale':0.6,
                                                          'ksp_max_it': 2,
                                                          'pc_type':'jacobi'}}}     
        elif (self.solver == 'hybridised_amg'):
            solver_param = {'ksp_type': 'preonly',
                            'mat_type': 'matfree',
                            'pc_type': 'python',
                            'pc_python_type': 'firedrake.HybridizationPC',
                            # Solver for the trace system
                            'hybridization': {'ksp_type': 'bcgs',
                                              'pc_type': 'hypre',
                                              'ksp_rtol': self._solver_rtol,
                                              'mg_levels': {'ksp_type': 'richardson',
                                                            'ksp_richardson_scale':0.6,
                                                            'ksp_max_it': 2,
                                                            'pc_type': 'jacobi'}}}
        elif (self.solver == 'hybridised_nonnested'):
            if (self.coarse_solver == 'exact'):
                coarse_param = {'ksp_type': 'preonly',
                                'pc_type': 'lu'}                
            elif (self.coarse_solver == 'amg'):
                coarse_param = {'ksp_type': 'preonly',
                                'pc_type': 'hypre',
                                'pc_gamg_sym_graph': True,
                                'mg_levels': {'ksp_type': 'richardson',
                                                          'ksp_max_it': 2,
                                                          'pc_type': 'bjacobi',
                                                          'sub_pc_type': 'ilu'}}
            elif (self.coarse_solver == 'gmg'):
                coarse_param = {'ksp_type': 'preonly',
                                'pc_type': 'mg',
                                'pc_mg_levels':2,
                                'pc_mg_cycles': 'v',
                                'mg_levels': {'ksp_type': 'chebyshev',
                                              'ksp_max_it': 2,
                                              'pc_type': 'bjacobi',
                                              'sub_pc_type': 'sor'},
                                              'mg_coarse': {'ksp_type': 'chebyshev',
                                                            'ksp_max_it': 2,
                                                            'pc_type': 'bjacobi',
                                                            'sub_pc_type': 'sor'}}
            else:
                print('ERROR: unknown coarse solver type: '+self.coarse_solver)            
            
            solver_param = {'ksp_type': 'preonly',
                            'mat_type': 'matfree',
                            'pc_type': 'python',
                            'pc_python_type': 'firedrake.HybridizationPC',
                            # Solver for the trace system
                            'hybridization': {'ksp_type': 'bcgs',
                                              'pc_type': 'python',
                                              'ksp_rtol': self._solver_rtol,
                                              'pc_python_type': 'firedrake.GTMGPC',
                                              'gt': {'mat_type': 'aij',
                                                     'pc_mg_log': None,
                                                     'mg_levels': {'ksp_type': 'chebyshev',
                                                                   #'ksp_richardson_scale':0.6,
                                                                   'ksp_max_it': 2,
                                                                   'pc_type': 'sor'},
                                                     'mg_coarse': coarse_param}}}
            
            if (self.coarse_space == 'P1'):

                # Define P1 coarse space and callback

                def get_coarse_space():
                    return FunctionSpace(self._mesh, 'CG', 1)

                def coarse_callback():
                    P1 = get_coarse_space()
                    q = TrialFunction(P1)
                    r = TestFunction(P1)
                    beta = Constant(alpha**2)
                    return (inner(q, r) + beta*self._bath*inner(grad(q), grad(r)))*self._dx

            elif (self.coarse_space == 'DG0'):

                # Define DG0 coarse space and callback

                def get_coarse_space():
                    return FunctionSpace(self._mesh, 'DG', 0)

                def coarse_callback():
                    DG0 = get_coarse_space()
                    phi = TrialFunction(DG0)
                    psi = TestFunction(DG0)
                    #### Need to work out refinement from grid!
                    nrefine = 4
                    h = 0.5**nrefine
                    return psi*phi*dx + Constant(alpha/h)*self._bath*(psi('+')-psi('-'))*(phi('+')-phi('-'))*dS
            else:
                print('ERROR: unknown coarse space: '+self.coarse_space)

            V_trace = FunctionSpace(self._mesh, "HDiv Trace", self._degree-1)
            interpolation_matrix = prolongation_matrix(V_trace,get_coarse_space())

            appctx = {'get_coarse_operator': coarse_callback,
                      'get_coarse_space': get_coarse_space,
                      'interpolation_matrix':interpolation_matrix}

        else:
            print('ERROR: unknown solver type: '+self.solver)

        # Initialize linear solver
        u, phi = TrialFunctions(self._V_q)
        mass_integral = self._resM(u, phi)
        S = self._resL(u, phi)
        bilinear = mass_integral - alpha*S
        lvp = LinearVariationalProblem(bilinear, r, q)
        lvs = LinearVariationalSolver(lvp,
                                      solver_parameters=solver_param,
                                      appctx=appctx)
        
        ksp_label = 'stage_'+str(len(self._ksp_monitors))
        ksp_monitor = KSPMonitor(label=ksp_label,
                                 comm=self._mesh.comm,
                                 verbose=self._ksp_verbose)
        self._ksp_monitors.append(ksp_monitor)

        # If hybridised also monitor trace convergence

        if 'hybridised' in self.solver:
            lvs.solve()
            trace_ctx = lvs.snes.ksp.pc.getPythonContext()
            trace_ctx.trace_ksp.setMonitor(self._ksp_monitors[-1])
        else:
            lvs.snes.ksp.setMonitor(self._ksp_monitors[-1])

        return WrappedSolver(lvs, self._ksp_monitors[-1], "Linear Solve")


    def mass_solver(self, r, q):
        '''Solver for the mass matrix :math:`Mq=r` for a given
        right hand side :math:`r` and solution :math:`q`.

        :arg r: residual :math:`r`
        :arg q: solution
        '''
        u, phi = TrialFunctions(self._V_q)
        # Solve using LU factorisation on the diagonal blocks
        mass_solver_param = {'ksp_type': 'preonly',
                             'pc_type': 'fieldsplit',
                             'pc_fieldsplit_type': 'additive',
                             'fieldsplit': {'ksp_type': 'preonly',
                                            'pc_type': 'bjacobi',
                                            'sub_pc_type': 'lu'}}

        S = self._resM(u, phi)
        lvp = LinearVariationalProblem(S, r, q)
        lvs = LinearVariationalSolver(lvp,
                                      solver_parameters=mass_solver_param)

        ksp_label = 'mass_solve'
        ksp_monitor = KSPMonitor(label=ksp_label,
                                 comm=self._mesh.comm,
                                 verbose=self._ksp_verbose)
        self._ksp_monitors.append(ksp_monitor)

        return WrappedSolver(lvs, self._ksp_monitors[-1], 'Mass Solve')
