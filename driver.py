from firedrake import *
import numpy as np
import time
import sys, os
import argparse
from mpi4py import MPI
from shallow_water import *
from ksp_monitor import *
from timestepper import *
from balanced_vortex import *
from williamson import *

class NormMonitor(TimeStepperMonitor):
    '''Class for printing out the norm of the potential in each iteration
    of the timestepper'''
    def __init__(self):
        pass

    def __enter__(self):
        pass

    def __call__(self,t,q):
        phi = q.split()[1]
        u = q.split()[0]
        mesh = u.function_space().mesh()
        X = SpatialCoordinate(mesh)
        print (('time = %8.4f' % t), (' int_{domain} phi(x) dx = %20.12e' % assemble(phi*dx)),end='')
        if (len(X)) == 3:
            print ('||u.hat(r)|| = %12.6e' % math.sqrt(assemble(dot(u,X)**2*dx)))
        else:
            print('')

def pprint(*s):
    '''Print string only on master process

    :arg s: Stuff to print
    '''
    if (MPI.COMM_WORLD.Get_rank() == 0):
        print (*s,flush=True)

comm_size = MPI.COMM_WORLD.Get_size()
pprint ('Running on '+str(comm_size)+' processors')

parameters["pyop2_options"]["lazy_evaluation"] = False
# Parse command line options
parser = argparse.ArgumentParser(allow_abbrev=False)
# Mesh geometry
parser.add_argument('--geometry',
                    choices=('spherical','flat'),
                    default='flat',
                    help='mesh geometry')

parser.add_argument('--degree',
                    type=int,
                    default=2,
                    help='polynomial degree of DG space')

parser.add_argument('--tfinal',
                    type=float,
                    default=1.0,
                    help='final time (in days)')

parser.add_argument('--mesh_degree',
                    type=int,
                    default=1,
                    help='polynomial degree of mesh')

parser.add_argument('--nrefine',
                    type=int,
                    default=3,
                    help='mesh refinement level, has to be > 1 for flat geometry')

parser.add_argument('--solver',
                    choices=('direct','pressure_multigrid','hybridised_amg','hybridised_nonnested'),
                    default='direct',
                    help='select solver to use for solving linear system')

parser.add_argument('--coarse_solver',
                    choices=(None,'exact','amg','gmg'),
                    default=None,
                    help='select coarse solver to use on nonnested coarse system')

parser.add_argument('--coarse_space',
                    choices=(None,'DG0','P1'),
                    default=None,
                    help='select coarse space to use on nonnested coarse system')

parser.add_argument('--nonlinear',
                    action='store_true',
                    default=False,
                    help='solve non-linear SWEs?')

parser.add_argument('--ksp_verbosity',
                    type=int,
                    default=0,
                    help='verbosity of KSP solvers. 0: no output, 1: summary, 2: full output')

parser.add_argument('--initial_condition',
                    choices=('gaussian','balanced_vortex', 'flat','solidbodyrotation','williamson_steady','williamson_steady_compact','williamson_mountain','williamson_rossbyhaurwitz'),
                    default='balanced_vortex',
                    help='initial condition; can be Gaussian bump (\'gaussian\') with zero momentum, balanced stationary vortex (\'balanced_vortex\'), flat surface with zero velocity (\'flat\'), solid body rotation (\'solidbodyrotation\'), Williamson steady-state zonal flow testcase (\'williamson_steady\'), Williamson steady-state zonal flow testcase with compact support (\'williamson_steady_compact\'), Williamson isolated mountain testcase (\'williamson_mountain\'), Williamson Rossby-Haurwitz testcase (\'williamson_rossbyhaurwitz\')')

args, unknown = parser.parse_known_args()

if (args.geometry == 'flat'):
    mesh = PeriodicUnitSquareMesh(2**args.nrefine, 2**args.nrefine,
                                  quadrilateral=True)
    # Number of cells and grid spacing
    ncell = 4**args.nrefine
    h = 0.5**args.nrefine
else:
    mesh = UnitCubedSphereMesh(refinement_level=args.nrefine,
                               degree=self._mesh_degree)
    mesh.init_cell_orientations(mesh)
    ncell = 6*4**args.nrefine
    h = np.sqrt(4.*np.pi/ncell)

if (args.initial_condition == 'williamson_steady'):
    williamson = WilliamsonSteadyStateZonal(mesh,True)
elif (args.initial_condition == 'williamson_steady_compact'):
    williamson = WilliamsonSteadyStateZonalCompact(mesh,True)
elif (args.initial_condition == 'williamson_mountain'):
    williamson = WilliamsonIsolatedMountain(mesh)
elif (args.initial_condition == 'williamson_rossbyhaurwitz'):
    williamson = WilliamsonRossbyHaurwitz(mesh)
else:
    williamson=None

sw_param = ShallowWaterParam(williamson=williamson)

# CFL scaling parameters. The explicit method is stable of approximately
# nu_{CFL} = c_g*dt/dx < \rho_{CFL}/(2*p+1)
# Here choose a time step size which is \alpha times larger than this, i.e.
# dt = \alpha*\rho_{CFL}/(2*p+1)*dx/c_g

rho_CFL = 0.5

# scaling factor alpha
alpha = 10.0

# Time step size
dt = alpha*rho_CFL*h/(2.*args.degree+1)/sw_param.cg

# Set up solver configuration dictionary

solver_configuration = {'solver' : args.solver,
                        'coarse_solver' : args.coarse_solver,
                        'coarse_space' : args.coarse_space}

if (args.solver == 'hybridised_nonnested'):
    if not args.coarse_solver:
        print('ERROR: need to specify coarse solver for: '+args.solver)
    if not args.coarse_space:
        print('ERROR: need to specify coarse space for: '+args.solver)


# Print out parameters of run
pprint (" geometry          = ", args.geometry)
pprint (" initial condition = ", args.initial_condition)
pprint (" non-linear?       = ", args.nonlinear)
pprint (" nrefine           = ", args.nrefine)
pprint (" degree p          = ", args.degree)
pprint (" mesh degree p     = ", args.mesh_degree)
pprint (" c_g               = ", sw_param.cg)
pprint (" f/c_g             = ", 2.*sw_param.omega/sw_param.cg)
pprint (" h                 = ", h)
pprint (" dt                = ", dt)
pprint (" alpha             = ", alpha)
pprint (" T                 = ", args.tfinal)
pprint (" #cells            = ", ncell)
pprint (" solver            = ", args.solver)

if (args.solver == 'hybridised_nonnested'):
    pprint(" coarse solver     = ", args.coarse_solver)
    pprint(" coarse space      = ", args.coarse_space)

# Print out number of processors
pprint(" Number of processes = ",mesh.comm.size)

# Need vortex earlier to specify bathymetry
f_over_cg = 2.0*sw_param.omega/sw_param.cg
dphi = 0.1
vortex = BalancedVortex(dphi,f_over_cg,args.nonlinear,bathymetry=True)

# Construct bathymetry
V_bath = FunctionSpace(mesh, 'CG', 1)
bathymetry = Function(V_bath, name='bathymetry')
if (args.initial_condition == 'williamson_mountain'):
    V = FunctionSpace(mesh, 'CG', 1)
    f_bathymetry = williamson.bathymetry(V)
elif args.geometry=='flat':
    x, y = SpatialCoordinate(mesh)
    dphi = 0.1
    if (args.initial_condition=='balanced_vortex'):
        f_bathymetry = vortex.bath_ufl(mesh)
    else:
        f_bathymetry = 1.0+dphi*cos(2*math.pi*x)*cos(2*math.pi*y)
else:
    f_bathymetry = 1.0

bathymetry.project(f_bathymetry)
outfile = File('output/bathymetry.pvd')
outfile.write(bathymetry)

if (args.initial_condition == 'solidbodyrotation') or (args.initial_condition == 'gaussian'):
    coriolis = False
else:
    coriolis = True

phi_exact = None
filename = 'output/solution.pvd'

sw = ShallowWater(args.geometry,
                  args.degree,
                  mesh,
                  bathymetry=f_bathymetry,
                  coriolis=coriolis,
                  nonlinear=args.nonlinear,
                  output_filename=filename,
                  ksp_verbose=args.ksp_verbosity,
                  solver_configuration=solver_configuration,
                  williamson=williamson)

# Construct initial condition
q = Function(sw.V_q)
u, phi = q.split()
u.assign(0.0)
if (args.geometry == 'flat'):
    # width of initial peak
    if (args.initial_condition=='balanced_vortex'):
        phi.project(vortex.phis_ufl(mesh))
        phi_exact  = phi.copy()
        u.project(vortex.u_ufl(mesh))
    elif (args.initial_condition=='gaussian'):
        x,y = SpatialCoordinate(mesh)
        sigma = 0.2
        phi.project(exp(-((x-0.5)**2+(y-0.5)**2)/sigma**2))
    elif (args.initial_condition=='flat'):
        phi.assign(0.1)
    else:
        pprint ('Invalid initial condition')
        sys.exit()
else:
    if (args.initial_condition=='solidbodyrotation'):
        # Solid body rotation test
        x,y,z = SpatialCoordinate(mesh)
        phi.assign(0.0)
        phi_exact  = phi.copy()
        u0 = 1.0
        u.project(as_vector((-u0*y,u0*x,0)))
    elif (args.initial_condition in ('williamson_steady',
                                     'williamson_steady_compact',
                                     'williamson_mountain',
                                     'williamson_rossbyhaurwitz')):
        if(args.bathymetry):
            pprint("INFO: --bathymetry flag ignored for this testcase")
        williamson.initial_condition(phi,u)
        if (args.initial_condition=='williamson_steady') or (args.initial_condition=='williamson_steady_compact'):
            phi_exact = phi.copy()
    else:
        pprint ('Invalid initial condition')
        sys.exit()

monitor=NormMonitor()

monitor = SaveToDiskMonitor('output/phi.pvd', field_idx=1, field_names=['phi'],interval=1,comm=None)

t_start = time.time()
if not (williamson is None):
    u_max = williamson.u_adv_max()
    pprint(f'u_max = {u_max}')
    pprint(f'h = {h}')
    dt = rho_CFL*h/(2.*args.degree+1)/u_max
    froude_max = williamson.Froude_max()
    pprint(f'maximal Froude number = '+('%4.3f' % froude_max))

timestepper = TimeStepperIMEXARS2_2_3_2(sw,dt,monitor)
with monitor:
    q_final = timestepper.integrate(q,args.tfinal)

phi_final = q_final.split()[1]
outfile = File('output/phi_final.pvd')
outfile.write(phi_final)

# Work out error
if not (phi_exact is None):
    phi_error = math.sqrt(assemble((phi_final-phi_exact)*(phi_final-phi_exact)*dx))
    pprint ('error in phi-field = '+('%12.4e' % phi_error))
    pprint ('')
    phi_error = assemble((phi_final-phi_exact)/(1+phi_exact))
    outfile = File('output/phi_rel_error.pvd')
    outfile.write(phi_error)
if not (williamson is None):
    V_bath = FunctionSpace(mesh,'CG',2)
    f_bath = Function(V_bath)
    f_bath.project(williamson.bathymetry_expr())
    outfile = File('output/bathymetry.pvd')
    outfile.write(f_bath)
