from firedrake import *
import numpy as np
from scipy import integrate
from numpy.polynomial.legendre import leggauss


class Williamson(object):
    '''Base class for tests from the Williamson et al. suite in
    Williamson, D.L., Drake, J.B., Hack, J.J., Jakob, R. and Swarztrauber,
    P.N., 1992: "A standard test set for numerical approximations to the
    shallow water equations in spherical geometry." Journal of Computational
    Physics, 102(1), pp.211-224.

    :arg mesh: Mesh to use
    '''
    def __init__(self, mesh, nonlinear):
        self._mesh = mesh
        self.a = 6.37122E6 # [m]
        self.Omega_coriolis = 2.*np.pi/(24*3600.) # [s^{-1}
        self.g_grav = 9.80616 # [m s^{-2}]

    @property
    def Rearth(self):
        '''Return earth radius'''
        return self.a

    def latlon_coords(self):
        '''Return lat-lon coordinates on mesh
        (copied from gusto/initialisation_tools.py
        '''
        x, y, z = SpatialCoordinate(self._mesh)
        unsafe = z/sqrt(x*x + y*y + z*z)
        safe = Min(Max(unsafe, -1.0), 1.0)  # avoid silly roundoff errors
        theta = asin(safe)  # latitude
        phi = atan_2(y, x)  # longitude
        return theta, phi

    def cartesian2latlon_coords(self,X):
        '''Return lat-lon coordinates on mesh
        (copied from gusto/initialisation_tools.py
        '''
        x, y, z = X
        unsafe = z/np.sqrt(x*x + y*y + z*z)
        safe = min(max(unsafe, -1.0), 1.0)  # avoid silly roundoff errors
        theta = np.arcsin(safe)  # latitude
        phi = np.arctan2(y, x)  # longitude
        return theta, phi

    def bathymetry_expr(self):
        return Constant(1)

    def interpolate_phi(self,f_phi):
        '''Set initial potential perturbation

        :arg f_phi: function to interpolate onto.
        '''
        f_phi.interpolate(self.phi_expr())

    def interpolate_u(self,u_phi):
        '''Set initial momentum

        :arg f_u: function to interpolate onto.
        '''
        u_phi.interpolate(self.u_expr())

    def initial_condition(self,f_phi,f_u):
        '''Set initial condition for potential perturbation and momentum

        :arg f_phi: function to interpolate potential perturbation onto.
        :arg f_phi: function to interpolate momentum onto.
        '''
        self.interpolate_phi(f_phi)
        self.interpolate_u(f_u)

    def u_adv_max(self):
        V_scalar = FunctionSpace(self._mesh,'CG',1)
        f_uadv = Function(V_scalar)
        f_uadv.interpolate(sqrt(inner(self.u_expr(),self.u_expr())))
        with f_uadv.dat.vec_ro as v:
            u_max = v.max()
        return u_max[1]

    def Froude_max(self):
        V_scalar = FunctionSpace(self._mesh,'CG',1)
        f_froude = Function(V_scalar)
        f_froude.interpolate(sqrt(inner(self.u_expr(),self.u_expr()))/(self.bathymetry_expr()+self.phi_expr()))
        with f_froude.dat.vec_ro as v:
            f_max = v.max()
        return f_max[1]

class WilliamsonSteadyStateZonal(Williamson):
    def __init__(self,mesh,nonlinear=True):
        '''Test #2: steady state zonal flow

        :arg mesh: Mesh to use
        :arg linear: Use linear approximation?
        '''
        super().__init__(mesh, nonlinear)
        self._nonlinear = nonlinear
        u0 = 2.*np.pi*self.a/(12*24*3600.) # [m s^{-1}]
        self.h0 = 3000. # depth [m]
        if (self._nonlinear):
            self._delta_phi_hat = (self.a*self.Omega_coriolis*u0 + 0.5*u0**2)/(self.g_grav*self.h0)
        else:
            self._delta_phi_hat = self.a*self.Omega_coriolis*u0/(self.g_grav*self.h0)
        self._u0_hat = u0/np.sqrt(self.g_grav*self.h0)

    def phi_expr(self):
        '''Return expression for potential perturbation'''
        x, y, z = SpatialCoordinate(self._mesh)
        return -self._delta_phi_hat*z**2

    def u_expr(self):
        '''Return expression for momentum'''
        x,y,z = SpatialCoordinate(self._mesh)
        if (self._nonlinear):
            U0 = self._u0_hat*(1.-self._delta_phi_hat*z**2)
        else:
            U0 = self._u0_hat
        return as_vector((-U0*y, U0*x, 0))


class WilliamsonSteadyStateZonalCompact(Williamson):
    def __init__(self,mesh,nonlinear=True):
        '''Test #3: steady state zonal flow with compact support

        :arg mesh: Mesh to use
        '''
        super().__init__(mesh, nonlinear)
        self._nonlinear = nonlinear
        u0 = 2.*np.pi*self.a/(12*24*3600.) # [m s^{-1}]
        self.h0 = 3000. # depth [m]
        self._u0_hat = u0/np.sqrt(self.g_grav*self.h0)
        self.x_e = 0.3
        self.theta_b = -np.pi/6.
        self.theta_e = +np.pi/2.

    def B(self,tau):
        '''Smooth function with compact support b(x)*b(x_e-x)*exp(4/x_e)
        defined in Eqs. (101) & (102) of Williamson et at.
        '''

        x = self.x_e*(tau-self.theta_b)/(self.theta_e-self.theta_b)
        return np.exp(-1/x)*np.exp(-1/(self.x_e-x))*np.exp(4./self.x_e)


    def B_expr(self,tau):
        '''UFL expression for the smooth function with compact support
        b(x)*b(x_e-x)*exp(4/x_e) defined in Eqs. (101) & (102) of
        Williamson et at.
        '''

        x = self.x_e*(tau-self.theta_b)/(self.theta_e-self.theta_b)
        return exp(-1/x)*exp(-1/(self.x_e-x))*np.exp(4./self.x_e)

    def phi_expr(self):
        '''UFL expression for phi'''

        def gauss_quadrature(x0, x1, function, deg=4):
            '''Compute Gauss quadrature to some degree for a function, assuming
            integration domain [x0, x1].

            Copied from galweski_test.py written by Golo Wimmer

            :arg x0: lower bound of integration domain
            :arg x1: upper bound of integration domain
            :arg function: function to integrate
            :arg degree: degree auf Gauss quadrature
            '''
            f = leggauss(deg)[1][0]*function(0.5*(x1 - x0)*leggauss(deg)[0][0]
                                            + 0.5*(x1 + x0))
            for i in range(1, deg):
                f += leggauss(deg)[1][i]*function(0.5*(x1 - x0)*leggauss(deg)[0][i]
                                                + 0.5*(x1 + x0))
            return 0.5*(x1 - x0)*f

        # Now use this to compute the integrals
        Omega_coriolis_a_hat = self.Omega_coriolis*self.a/np.sqrt(self.g_grav*self.h0)

        if (self._nonlinear):
            delta_NL = 1
        else:
            delta_NL = 0
        # Integrand
        psi = lambda tau: -self._u0_hat*(2.*Omega_coriolis_a_hat*np.sin(tau)+delta_NL*self._u0_hat*self.B(tau)*np.tan(tau))*self.B(tau)
        # Integrand as a UFL expression
        psi_expr = lambda tau: -self._u0_hat*(2.*Omega_coriolis_a_hat*sin(tau)+delta_NL*self._u0_hat*self.B_expr(tau)*tan(tau))*self.B_expr(tau)

        # Integrate numerically from theta_b to theta_e to get the value of
        # phi at the upper limit of the integration interval
        phi_e = integrate.quad(psi,self.theta_b,self.theta_e)[0]

        theta, lamda = self.latlon_coords()
        return conditional(theta <= self.theta_b, 0,
                            conditional(theta > self.theta_e, phi_e,
                                        gauss_quadrature(self.theta_b, theta,
                                                            psi_expr,
                                                            deg=64)))

    def u_expr(self):
        '''Return expression for momentum'''
        theta, lamda = self.latlon_coords()
        if (self._nonlinear):
            U0 = self._u0_hat*(1+self.phi_expr())
        else:
            U0 = self._u0_hat
        f_compact = conditional(theta <= self.theta_b, 0,
                                conditional(theta >= self.theta_e, 0,
                                            self.B_expr(theta)))
        return as_vector((-U0*f_compact*sin(lamda), U0*f_compact*cos(lamda), 0))

class WilliamsonIsolatedMountain(Williamson):
    def __init__(self,mesh,nonlinear=True):
        '''Test #5: zonal flow over isolated mountain

        :arg mesh: Mesh to use
        '''
        super().__init__(mesh, nonlinear)
        u0 = 2.*np.pi*self.a/(12*24*3600.) # [m s^{-1}]
        self.h0 = 5400. # reference depth [m]
        hs0 = 2000. # mountain height [m]
        self._delta_phi_hat = (self.a*self.Omega_coriolis*u0 + 0.5*u0**2)/(self.g_grav*self.h0)
        self._u0_hat = u0/np.sqrt(self.g_grav*self.h0)
        self._h_hat = hs0/self.h0
        # width of mountain
        self.R = pi/9.
        # lat-lon coordinates of mountain
        self.lamda_c = -np.pi/2.
        self.theta_c = np.pi/6.

    def bathymetry_expr(self):
        '''Return expression for bathymetry'''
        theta, lamda = self.latlon_coords()
        # Location of mountain
        Rsq = self.R**2
        rsq = Min(Rsq, (lamda - self.lamda_c)**2 + (theta - self.theta_c)**2)
        r = sqrt(rsq)
        return 1 - self._h_hat * (1 - r/self.R)

    def phi_expr(self):
        '''Return expression for potential perturbation'''
        x,y,z = SpatialCoordinate(self._mesh)
        return -self._delta_phi_hat*z**2

    def u_expr(self):
        '''Return expression for momentum'''
        x,y,z = SpatialCoordinate(self._mesh)
        U0 = self._u0_hat*(self.bathymetry_expr()-self._delta_phi_hat*z**2)
        return as_vector((-U0*y,U0*x,0))

    def bathymetry(self, f_bath):
        f_bath.interpolate(self.bathymetry_expr())

    def generate_mountain_vtk(self,filename):
        '''Generate vtk file with circle depicting mountain'''
        R_y = np.array([[np.sin(self.theta_c),0,-np.cos(self.theta_c)],
                        [0,1,0],
                        [np.cos(self.theta_c),0,np.sin(self.theta_c)]])
        R_z = np.array([[np.cos(self.lamda_c),np.sin(self.lamda_c),0],
                        [-np.sin(self.lamda_c),np.cos(self.lamda_c),0],
                        [0,0,1]])
        def xyz(rho,phi):
            '''Return 3d coordinates for points on circle that represents
            base of the mountain'''
            R_0 = 1.0
            v = np.array([R_0*np.sin(rho*self.R)*np.sin(phi),
                          R_0*np.sin(rho*self.R)*np.cos(phi),
                          R_0*np.cos(rho*self.R)]).transpose()
            return R_z @ R_y @ v
        n_points = 64
        n_contour = 2
        with open(filename,'w') as f:
            preamble = '''# vtk DataFile Version 3.0
            vtk output
            ASCII
            DATASET POLYDATA
            '''
            print (preamble,file=f)
            print (f'POINTS {(n_contour+1)*(n_points+1)} double',file=f)
            for k in range(n_contour+1):
                for j in range(n_points+1):
                    phi = j*2.*np.pi/n_points
                    if (k==0):
                        radius = 0.05
                    else:
                        radius = k/n_contour
                    p = xyz(radius,phi)
                    print (('%8.5f' % p[0])+' '+('%8.5f' % p[1])+' '+('%8.5f' % p[2]),file=f)
            print(f'POLYGONS {n_contour+1} {(n_contour+1)*(n_points+2)}',file=f)
            for k in range(n_contour+1):
                print(f' {n_points+1}',end='',file=f)
                for j in range(n_points+1):
                    print(f' {k*(n_points+1)+j}',end='',file=f)
                print('',file=f)

class WilliamsonRossbyHaurwitz(Williamson):
    def __init__(self,mesh,nonlinear=True):
        '''Test #6: Rossby-Haurwitz wave

        :arg mesh: Mesh to use
        '''
        super().__init__(mesh, nonlinear)
        self.h0 = 8000. # reference depth [m]
        omega = 7.484E-6 # [s^{-1}]
        K = 7.484E-6 # [s^{-1}]
        self.R = 4
        # omega*a/\sqrt{g*h_0}
        self._a_omega_hat = self.a*omega/np.sqrt(self.g_grav*self.h0)
        # K*a/\sqrt{g*h_0}
        self._a_K_hat = self.a*K/np.sqrt(self.g_grav*self.h0)
        # Omega*a/\sqrt{g*h_0}
        self._a_Omega_coriolis_hat = self.a*self.Omega_coriolis/np.sqrt(self.g_grav*self.h0)

    def phi_expr(self):
        '''Return expression for potential perturbation'''
        theta, lamda = self.latlon_coords()
        A = self._a_omega_hat/2*(2*self._a_Omega_coriolis_hat+self._a_omega_hat)*cos(theta)**2 + self._a_K_hat**2/4*cos(theta)**(2*self.R-2)*((self.R+1)*cos(theta)**4+(2*self.R**2-self.R-2)*cos(theta)**2-2*self.R**2)
        B = 2*(self._a_Omega_coriolis_hat+self._a_omega_hat)*self._a_K_hat/((self.R+1)*(self.R+2))*cos(theta)**self.R*((self.R**2+2*self.R+2)-(self.R+1)**2*cos(theta)**2)
        C = self._a_K_hat**2/4*cos(theta)**(2*self.R)*((self.R+1)*cos(theta)**2-(self.R+2))
        return A + B*cos(self.R*lamda) + C*cos(2*self.R*lamda)

    def u_expr(self):
        '''Return expression for momentum'''
        theta, lamda = self.latlon_coords()
        U0 = (1.+self.phi_expr())*(self._a_omega_hat*cos(theta) + self._a_K_hat*cos(theta)**(self.R-1)*(self.R*sin(theta)**2-cos(theta)**2)*cos(self.R*lamda))
        V0 = -(1.+self.phi_expr())*self._a_K_hat*self.R*cos(theta)**(self.R-1)*sin(theta)*sin(self.R*lamda)
        Ux = -U0*sin(lamda)-V0*sin(theta)*cos(lamda)
        Uy = U0*cos(lamda)-V0*sin(theta)*sin(lamda)
        Uz = V0*cos(theta)
        return as_vector((Ux,Uy,Uz))


williamson_test = {}
williamson_test[2] = WilliamsonSteadyStateZonal
williamson_test[3] = WilliamsonSteadyStateZonalCompact
williamson_test[5] = WilliamsonIsolatedMountain
williamson_test[6] = WilliamsonRossbyHaurwitz


###############################################################
#      M A I N    (use for testing)
###############################################################
if (__name__ == '__main__'):
    # Save fields files for testing
    mesh = UnitIcosahedralSphereMesh(4)
    x_mesh = SpatialCoordinate(mesh)
    mesh.init_cell_orientations(x_mesh)

    # Mesh and function spaces
    V_phi = FunctionSpace(mesh,"CG",1)
    V_u = VectorFunctionSpace(mesh,"DG",1)

    # --- Testcase #2: Steady-state zonal flow ---
    ws_test = WilliamsonSteadyStateZonal(mesh,True)

    phi = Function(V_phi,name="potential")
    u = Function(V_u,name="momentum")
    ws_test.initial_condition(phi,u)

    outfile = File('williamson_steadystate.pvd')
    outfile.write(phi,u)

    # --- Testcase #3: Steady-state zonal flow with compact support ---
    ws_test = WilliamsonSteadyStateZonalCompact(mesh)

    phi = Function(V_phi,name="potential")
    u = Function(V_u,name="momentum")
    ws_test.initial_condition(phi,u)

    outfile = File('williamson_steadystate_compact.pvd')
    outfile.write(phi,u)

    # --- Testcase #5: Isolated mountain  ---
    ws_test = WilliamsonIsolatedMountain(mesh)

    phi = Function(V_phi,name="potential")
    u = Function(V_u,name="momentum")
    bathymetry = Function(V_phi,name="bathymetry")
    ws_test.initial_condition(phi,u)
    ws_test.bathymetry(bathymetry)

    outfile = File('williamson_mountain.pvd')
    outfile.write(phi,u,bathymetry)

    ws_test.generate_mountain_vtk('mountain.vtk')

    # --- Testcase #6: Rossby-Haurwitz wave  ---
    ws_test = WilliamsonRossbyHaurwitz(mesh)

    phi = Function(V_phi,name="potential")
    u = Function(V_u,name="momentum")
    ws_test.initial_condition(phi,u)

    outfile = File('williamson_rossbyhaurwitz.pvd')
    outfile.write(phi,u)
