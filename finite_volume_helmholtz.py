from firedrake import *
import numpy as np
import argparse

'''
===========================================================================
    Solve the Helmholtz equation \phi - \alpha*\Laplace\phi = f_rhs
    using finite volumes in Firedrake.
===========================================================================
'''

def analytical_expressions(mesh,alpha):
    '''Analytical expressions for the solution and corresponding right hand
    side. The solution is written as a Fourier-cosine series with arbitrary
    coefficients and shifts.

    :arg mesh: Underlying mesh
    :arg alpha: parameter alpha in PDE
    '''
    x, y = SpatialCoordinate(mesh)
    expr_sol = 0
    expr_rhs = 0
    for kx in range(3):
        for ky in range(3):
            eigenval = (1+alpha*4*pi*pi*(kx**2+ky**2))
            coeff = np.random.uniform(low=0,high=1)
            shiftx = np.random.uniform(low=0,high=2*np.pi)
            shifty = np.random.uniform(low=0,high=2*np.pi)
            expr_sol += coeff*cos(x*2*pi*kx+shiftx)*cos(y*2*pi*ky+shifty)
            expr_rhs += coeff*eigenval*cos(x*2*pi*kx+shiftx)*cos(y*2*pi*ky+shifty)
    return expr_sol, expr_rhs

# Set arbitrary random seed
np.random.seed(1341517)

# Parse command line arguments
parser = argparse.ArgumentParser(allow_abbrev=False)
parser.add_argument('--nrefine',
                    type=int,
                    default=3,
                    help='mesh refinement level, has to be > 0')
args, _ = parser.parse_known_args()

# Relative size of second order and zero order terms
alpha = 1.0

# Create mesh with quadrilateral elements
mesh = PeriodicUnitSquareMesh(2**args.nrefine,2**args.nrefine,
                              quadrilateral=True)

# Work out spacing, this is needed to construct the correct scaling for
# the second order coefficient in the finite volume discretisation
h = 0.5**args.nrefine

# Construct functionspace and trial-/test-functions
V = FunctionSpace(mesh,'DG',0)

phi = TrialFunction(V)
psi = TestFunction(V)

# Construct function storing the right hand side and project analytical
# expression to it
f_rhs = Function(V)
expr_sol, expr_rhs  = analytical_expressions(mesh,alpha)
f_rhs.project(expr_rhs)

# 2-form
a = psi*phi*dx + Constant(alpha/h)*(psi('+')-psi('-'))*(phi('+')-phi('-'))*dS

# 1-form for right hand side
L = psi*f_rhs*dx

# Exact solution
u_exact = Function(V,name='exact solution').project(expr_sol)

# Construct numerical solution by solving the linear equation
u = Function(V,name='numerical solution')
solve(a == L, u, solver_parameters={'ksp_type': 'preonly',
                                    'pc_type': 'lu'})

# Work out error
error = Function(V,name='error')
error.assign(u-u_exact)

# Print error (to monitor convergence rate)
print('refinement = ',args.nrefine)
print('||u-u_{exact}|| = '+('%8.4e' % norm(error)))

# Save all functions to a .pvd file
outfile = File("solution.pvd")
outfile.write(u,u_exact,error)
